package main

import "fmt"

type person struct {
	firstName string
	lastName  string
	likes     []string
}

func main() {
	aaron := person{
		firstName: "Aaron",
		lastName:  "Peterson",
		likes:     []string{"computers", "making", "programming"},
	}
	jaimie := person{
		firstName: "Jaimie",
		lastName:  "Peterson",
		likes:     []string{"netflix", "soap", "outside"},
	}

	fmt.Println(aaron)
	fmt.Println(jaimie)

	m := map[string]person{
		aaron.firstName:  aaron,
		jaimie.firstName: jaimie,
	}

	for k, v := range m {
		fmt.Println(k)
		for i, val := range v.likes {
			fmt.Printf("%v %v\n", i, val)
		}
	}
}
