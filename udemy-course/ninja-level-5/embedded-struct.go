package main

import "fmt"

type vehicle struct {
	doors int
	color string
}
type truck struct {
	vehicle
	fourWheel bool
}
type sedan struct {
	vehicle
	luxury bool
}

func main() {

	silverado := truck{
		vehicle: vehicle{
			doors: 2,
			color: "silver",
		},
		fourWheel: true,
	}

	fit := sedan{
		vehicle: vehicle{
			doors: 4,
			color: "white",
		},
		luxury: false,
	}

	fmt.Println(silverado)
	fmt.Println(silverado.color)
	fmt.Println(fit)
	fmt.Println(fit.doors)

}
