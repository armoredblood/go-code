package main

import "fmt"

func main() {
	a := [5]int{2, 4, 6, 8, 10}
	for i, v := range a {
		fmt.Println(i, v)
	}
	fmt.Printf("%T\n", a)

}
