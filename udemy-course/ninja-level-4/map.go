package main

import "fmt"

func main() {
	m := make(map[string][]string)
	m["James"] = []string{"shaken not stirred", "martinis", "women"}
	m["Moneypenny"] = []string{"James Bond", "Money", "computers"}
	m["dr_no"] = []string{"being evil", "ice cream", "sunsents"}

	// m1 := map[string][]string{
	// 	"James":      []string{"shaken not stirred", "martinis", "women"},
	// 	"Moneypenny": []string{"James Bond", "Money", "computers"},
	// 	"dr_no":      []string{"being evil", "ice cream", "sunsents"},
	// }

	delete(m, "dr_no")

	for _, x := range m {

		fmt.Printf("slice: %v\n", x)
		for ii, s := range x {
			fmt.Printf("%v %v\n", ii, s)
		}
	}
}
