package main

import "fmt"

func main() {
	s := [][]string{
		{"james", "bond", "shaken not stirred"},
		{"miss", "moneypenny", "hellooooo james"},
	}

	for _, v := range s {
		for _, vv := range v {
			fmt.Println(vv)
		}
	}
}
