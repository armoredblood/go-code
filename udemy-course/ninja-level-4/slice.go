package main

import "fmt"

func main() {
	s := []int{42, 43, 44, 45, 46, 47, 48, 49, 50}

	for i, v := range s {
		fmt.Println(i, v)
	}
	fmt.Printf("%T\n", s)

	s1 := s[:5]  //[42 43 44 45 46]
	s2 := s[4:]  //[46 47 48 49 50]
	s3 := s[2:7] //[44 45 46 47 48]
	s4 := s[1:6] // [43 44 45 46 47]

	fmt.Println(s1)
	fmt.Println(s2)
	fmt.Println(s3)
	fmt.Println(s4)

	fmt.Printf("%T\n", s1)

	s5 := []int{56, 57, 58, 59, 60}

	s = append(s, 51)
	s = append(s, 52, 53, 54)
	s = append(s, s5...)

	fmt.Println(s)

	// delete from slice by slicing
	s = append(s[:3], s[6:10]...)

	fmt.Println(s)

}
