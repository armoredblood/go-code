package main

import (
	"fmt"
	"time"
)

var result int

func main() {

	max := 1000
	var average int64

	for j := 0; j <= 100; j++ {
		start := time.Now()
		for i := 0; i <= max; i++ {
			result = factorial(20)
			fmt.Println(result)
		}
		loopDuration := time.Since(start)
		average += loopDuration.Milliseconds()
	}
	average = average / 100

	fmt.Println("The average runtime is ", average, "ms")
}

func factorial(n int) int {
	if n == 0 {
		return 1
	}
	return n * factorial(n-1)
}

func loopFact(n int) int {
	total := 1
	for ; n > 0; n-- {
		total *= n
	}
	return total
}
