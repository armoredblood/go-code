package main

import "fmt"

func main() {
	x := func() int {
		return 123
	}
	fmt.Println(x())
	fmt.Println(x())
}
