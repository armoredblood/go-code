package main

import (
	"fmt"
	"time"
)

const max int = 99999999

var x []int // dynamically sized slice
var y = make([]int, 10, max)
var z = make([]int, max, max)
var a [max]int
var b []int

func main() {

	// use dynamically sized slices and measure how long it takes to run
	start := time.Now()
	for i := 0; i <= max; i++ {
		x = append(x, i)
	}
	duration := time.Since(start)
	fmt.Printf("Runtime for dynamic slice is %s ms \n", fmt.Sprint(duration.Milliseconds()))

	// use pre-allocated sized slice via make() and measure how long it takes to run
	start = time.Now()
	for i := 0; i <= max; i++ {
		y = append(y, i)
	}
	duration = time.Since(start)
	fmt.Printf("Runtime for pre-allocated slice via make is %s ms \n", fmt.Sprint(duration.Milliseconds()))

	// use pre-allocated sized slice via make() and measure how long it takes to run
	start = time.Now()
	for i := 0; i < max; i++ {
		z[i] = i
	}
	duration = time.Since(start)
	fmt.Printf("Runtime for pre-allocated slice via make and no appends is %s ms \n", fmt.Sprint(duration.Milliseconds()))

	// use pre-allocated sized slice via make() and measure how long it takes to run
	start = time.Now()
	for i := 0; i < max; i++ {
		a[i] = i
	}
	duration = time.Since(start)
	fmt.Printf("Runtime for pre-allocated slice and no appends is %s ms \n", fmt.Sprint(duration.Milliseconds()))

	// use pre-allocated sized slice via make() and measure how long it takes to run
	start = time.Now()
	for i := 0; i < max; i++ {
		b[i] = i
	}
	duration = time.Since(start)
	fmt.Printf("Runtime for pre-allocated slice (dynamically sized) and no appends is %s ms \n", fmt.Sprint(duration.Milliseconds()))
}
